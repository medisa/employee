import React, { useEffect, useState } from "react";
import { Row, Col, Container } from "reactstrap";
import { connect } from "react-redux";
import { apiActions } from "./../_actions";
import employee01 from "./../assets/images/employee01.png";

const EmployeeItems = (props) => {
  const [isHover, setIsHover] = useState(false);

  useEffect(() => {}, [props.data]);

  const handleMouseHover = () => {
    setIsHover(toggleHoverState);
  };

  const toggleHoverState = () => {
    setIsHover(!isHover);
  };

  const addToCompare = (data) => {
    props.onGetItem(data);
  };

  return (
    <>
      <Col sm={6} lg={3} md={4} className="mt-2">
        <div className="info_box_wrapper">
          <Row onMouseEnter={handleMouseHover} onMouseLeave={handleMouseHover}>
            <a onClick={() => addToCompare(props.data)}>
              <img
                style={
                  isHover
                    ? {
                        opacity: 0.7,
                        transition: ".5s ease",
                        backfaceVisibility: "hidden",
                      }
                    : {}
                }
                src={employee01}
              />
            </a>
          </Row>
          <Row>
            <Col sm={6} lg={8} md={8}>
              <Row className="ml-2">
                <p className="name_box_wrapper">{props.data.employee_name}</p>
              </Row>
              <Row className="ml-2">
                <p className="age_box_wrapper">{props.data.employee_salary}</p>
              </Row>
            </Col>
            <Col sm={6} lg={4} md={4} className="salary_box_wrapper">
              <p>{props.data.employee_age}</p>
            </Col>
          </Row>
        </div>
      </Col>
    </>
  );
};

function mapStateToProps(state) {
  return {
    getAllEmployees: state.api.getAllEmployees,
  };
}
export default connect(mapStateToProps)(EmployeeItems);
// export default EmployeesList;
