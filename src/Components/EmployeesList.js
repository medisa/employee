import React, { useEffect } from "react";
import { Row, Col, Container } from "reactstrap";
import { connect } from "react-redux";
import { apiActions } from "./../_actions";
import EmployeeItems from "./../Components/EmployeeItems";

const EmployeesList = (props) => {
  useEffect(() => {
    const { dispatch } = props;
    dispatch(apiActions.getAllEmployees());
    console.log();
  }, []);

  const handleGetItem = (item) => {
    props.onGetItem(item);
  };



  return (
    <>
      <Container className="my-5">
        <Row className="card_container_bg m-4">
          {props.getAllEmployees && props.getAllEmployees !== 0
            ? props.getAllEmployees.data.map((item, i) => {
                return (
                  <EmployeeItems onGetItem={handleGetItem} data={item} />
                );
              })
            : null}
        </Row>
      </Container>
    </>
  );
};

function mapStateToProps(state) {
  return {
    getAllEmployees: state.api.getAllEmployees,
  };
}
export default connect(mapStateToProps)(EmployeesList);
// export default EmployeesList;
