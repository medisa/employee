import React from "react";
import { Card, CardBody, Table, Container } from "reactstrap";

const EmployeeCompareGrid = (props) => {
  return (
    <>
      <Container className="my-5">
        <Card className="inquiry_result">
          <CardBody className="py-3">
            <p className="cards_title"></p>
            <Table striped responsive size="sm" key={props.compareItem.id}>
              <tr>
                <td className="text-center td_title">Name</td>{" "}
                <td className="text-center td_detail">
                  {props.compareItem.employee_name}
                </td>
              </tr>
              <tr>
                <td className="text-center td_title">Salary</td>
                <td className="text-center td_detail">
                  {props.compareItem.employee_salary}
                </td>
              </tr>
              <tr>
                <td className="text-center td_title">Age</td>
                <td className="text-center td_detail">
                  {props.compareItem.employee_age}
                </td>
              </tr>
            </Table>
          </CardBody>
        </Card>
      </Container>
    </>
  );
};

export default EmployeeCompareGrid;
