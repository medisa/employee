import React, { useState } from "react";
import EmployeesList from "./../Components/EmployeesList";
import EmployeeCompareGrid from "./../Components/EmployeeCompareGrid";

const Employee = (props) => {
  const [compareItem, setCompareItem] = useState("");
  const handleGetItem = (item) => {
    setCompareItem(item);
  };

  return (
    <>
      <EmployeesList onGetItem={handleGetItem} />
      <EmployeeCompareGrid compareItem={compareItem} />
    </>
  );
};

export default Employee;
