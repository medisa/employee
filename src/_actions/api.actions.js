import { allConstants } from "../_constants";
import axios from "axios";

export const apiActions = {
  getAllEmployees,
};

function getAllEmployees() {
  return (dispatch) => {
    dispatch(request());
    axios
      .get(`http://dummy.restapiexample.com/api/v1/employees`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        console.log(res);
        if (res.data.status === "success") {
          dispatch({
            type: allConstants.GET_ALL_EMPLOYEES,
            data: res.data,
          });
          return;
        }
        else {
          return;
        }
      })
      .catch((error) => {
        console.log(error);
      });

    function request(res) {
      return { type: allConstants.GET_ALL_EMPLOYEES, res };
    }
  };
}
