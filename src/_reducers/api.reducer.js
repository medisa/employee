import { allConstants } from "../_constants";
const initialState = [
  {
    getAllEmployees: "",
  },
];
export function api(state = initialState, action) {
  switch (action.type) {
    case allConstants.GET_ALL_EMPLOYEES:
      return {
        ...state,
        getAllEmployees: action.data,
      };

    default:
      return state;
  }
}
